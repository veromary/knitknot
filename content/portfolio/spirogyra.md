---
title: Spirogyra Wristwarmers
date: 2021-07-22 22:49:20 -0400
finish: 2021-07-27
image: spirogyra-1.jpg
nophotos: 1
tags: ["mittens"]
---

This winter I have chillblains on my right hand so settled on making emergency mittens. So far it seems to help.

**Pattern:** [Spirogyra](https://knitty.com/ISSUEspring08/KSPATTspirogyra.html)


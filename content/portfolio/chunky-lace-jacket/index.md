---
title: Chunky Lace Jacket
date: 2021-09-29
image: chunky-lace-jacket-1.jpg
nophotos: 2
needlesize: "9mm, 8mm"
patternname: "Lace Jacket"
---

I'm having fun with this.

Huge needles.

My mother asked what was the biggest thing I had knitted, as so many of my projects are tiny. She had passed on to me a semi-finished jacket in Patons Jet, Wool &amp; Alpaca, 30% Alpaca, 70% Wool. I started sewing it up, but it turned out with puffy shoulders, so I ended up frogging the lot.

So, now I'm working on my first jacket for myself!

It goes against my resolve never to become attached to a hand wash only garment, but so far another similar composition jumper which I finished off from a similar passed on project has survived, so maybe this shows a new phase of my life when I can once again enjoy sensitive fibre garments.

The pattern is a freebie I found through Love Crafts: ["Lace Jacket" - Jacket Knitting Pattern For Women in Debbie Bliss Winter Garden](https://www.lovecrafts.com/en-au/p/lace-jacket-jacket-knitting-pattern-for-women-in-debbie-bliss-winter-garden). I like how long it is. With this heavy yarn, a lace pattern may help the available yarn go further and not be so bulky as a more tightly knit garment.

I went and bought a new circular needle(s) especially for this. The pattern says to do the ribbing and the waist with 8mm needles and I had some straight 7.5mms, which was fine until it came to picking up over 100 sts along the front edge then I needed something longer. This was just before our Premier allowed unvaccinated people to access frivolous, unnecessary luxuries like knitting shops, so it was the first time I had the opportunity to present my [Vaccine Control Group Card](https://vaccinecontrolgroup.org).

The buttons are from my one and only overseas trip, being convinced to join in a service trip to the Philippines in my university days. So far they were too big for anything I came up with. They're actually a little small for the buttonholes, but I consider it a safety release feature which should prevent the fabric being strained excessively.


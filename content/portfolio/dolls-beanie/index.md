---
title: Doll's Beanie
date: 2021-04-17 23:45:11 -0400
finish: 2021-04-27
image: dolls-beanie-1.jpg
nophotos: 1
patternname: ""
needlesize: 2
tags: ["hats","doll clothes"]
---

I cast on 88 sts, planning to make a sock. I was going by the instructions that came with my Addi Crazy Trios. The wool is a special sock wool from Germany, 75% wool, 25% nylon.

Here's the beginning:

![Not how I expected](started.jpg)

The pattern is one I had used for a purple pair of socks in DK wool. The pattern seemed to disappear in the finer, darker wool.

![Vine Lace Pattern](/patterns/Vine-Lace-Pattern.png)



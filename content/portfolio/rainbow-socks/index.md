---
title: Rainbow Socks
date: 2021-01-24 17:28:46 -0400
finish: 2021-03-18
image: rainbow-socks.jpg
nophotos: 1
patternname: ""
needlesize: 
tags: [ "socks" ]
---

I cast on 60sts for these socks. The rainbow is sock wool Jawoll Twin, Lang Yarns (75% wool, 25% nylon). The green is an old Paton's Azalea, 100% Pure New Wool, Patonised for maximum resistance to shrinkage and pilling.

And here's the lace pattern:

![Waffle Rib](/patterns/Waffle-Rib.png)

I gleaned it from *Traditional Knitting Patterns* by James Gibb, page 110. It was in the German section.

I used my Addi Crazy Trios. I still have to measure them so I can say what size they are. I guess they're 3mm.


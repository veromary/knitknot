---
title: Bargain Socks
date: 2019-11-09 23:45:11 -0400
finish: 2019-11-22
image: bargain-sock.jpg
nophotos: 1
patternname: ""
needlesize: 
tags: ["socks"]
---

[Bendigo Woollen Mills](https://www.bendigowoollenmills.com.au/) had some cheap acrylic self striping yarn when we visited. It was a tough decision. Buy one ball of something really nice, or four balls of self striping cheap stuff. They had three colourways:

* magenta-orange-green-blue, 
* yellow-orange-pink-grey
* purple-pink

![socks](progress1.jpg)
![socks](progress2.jpg)
![socks](progress3.jpg)
![socks](progress4.jpg)

There was a break in the yarn on the second sock which mucked up the stripes.

![socks](othersock1.jpg)
![socks](othersock2.jpg)

The stripes matched up much better with the yellow yarn.


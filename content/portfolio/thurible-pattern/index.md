---
title: Thurible Pattern
date: 2023-06-01 17:28:46 -0400
finish: 2023-06-15
image: thurible-pattern.jpg
nophotos: 1
needlesize: "3.25mm"
tags: [ "toy", "liturgical" ]
---

I'm a gonna git this pattern down on paper, this time for sure!

**Pattern:** [draft-update](draft-update.pdf)

I'm using LaTeX knitting packages to set the pattern up.

And I need to knit more prototypes!


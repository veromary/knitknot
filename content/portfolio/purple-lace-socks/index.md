---
title: Purple Lace Socks
date: 2019-10-31 23:45:11 -0400
finish: 
image: purple-lace-sock.jpg
nophotos: 1
patternname: ""
needlesize: 
tags: ["socks"]
---

Trying to figure out what sized needles I used. I think I used 3.75mm for the cuffs and then 4mm for the rest of the sock. I used 48sts and this lace pattern:

![Vine Lace Pattern](/patterns/Vine-Lace-Pattern.png)

Here's the beginning:

![First Heel](one-heel-done.jpg)




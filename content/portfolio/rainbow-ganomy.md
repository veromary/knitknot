---
title: Rainbow Ganomy
date: 2021-05-31 22:49:20 -0400
finish: 2021-06-01
image: rainbow-ganomy-1.jpg
nophotos: 3
patternname: "EZ Ganomy Hat"
needlesize: 4
tags: ["hats","ganomy"]
---

As a break from sock knitting, I used up some balls of merino wool superwash (8ply) in this Ganomy Hat. The balls were leftovers from my mother - less than a whole ball of each colour.

I've always found it a bit of a gamble as to how many stitches to cast on. The shaping kinda eats up the width. This was with 100sts and fit a four year old.

I divided it 17:33:33:17 for the sections between the shapings.

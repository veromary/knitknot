---
title: Green Lattice Socks
date: 2021-08-25 03:45:11 -0400
finish: 2021-09-27
image: green-lattice-socks-1.jpg
nophotos: 5
patternname: ""
needlesize: "2.75mm"
tags: ["socks"]
---

Using my Crazy Trio sock needles, and I started off fudging the cables, but ended up using a cable needle.

64 sts around.

The trellis pattern is [Basic Lattice from Knitting Fool](https://www.knittingfool.com/StitchIndex/StitchDetail.aspx?StitchID=966)

![Basic Lattice](stitchfiddle.png)




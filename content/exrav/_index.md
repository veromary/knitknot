+++
date = "2021-07-01"
title = "Exit Ravelry"
+++

I'm not sure when the Ravelry ethos started to get to me. June 2017 I managed to suppress their logo, so I wouldn't have to see it when I logged in on my laptop. September-November 2017 was particularly grating, with the Australia Marriage Law Postal Survey bringing lots of campaigning to the fore. Wordpress targetted Australia for rainbow admin bars. Over time I became less and less comfortable using Ravelry's platform and my last post there is from June 2019.

The last straw came with their [No Trump Policy](https://www.ravelry.com/content/no-trump). 

Now, I'm living over the other side of the world from the US, but from my point of view, a policy which forbids mentioning anything positive about your current head of state is way off the rails. That's utter madness.

So, it took me a few months, but in August 2020 I decided to pull the plug, pull all my contributed photos, download my data and delete my account.

The download is a zip containing photos and json files. I hadn't done much with json files before, so it sat there for some time.

Finally, during the June/July lockdown of 2021, I finished my second RGB sock and wanted to know when I had knitted the first one. So I went searching for the zip file (handily named export-202008280400.zip - after some hours searching for every combination of rav, Rav, etc) and started messing about with bash scripts and Hugo templates to make this webpage.

The script to take the .json files and turn them into .md files is here:

{{% code file="/data/script" language="bash" %}}

Which will impress people who don't dig bash scripting and maybe horrify people who do.

In tandem with that, I set up the default Hugo template on Gitlab and chose the [Hugo Creative Portfolio Theme](https://github.com/kishaningithub/hugo-creative-portfolio-theme) and after two days preoccupied with this have something that looks decent.

Now I can bring the collection up to date and get back to adding to it.

+++
date = "2021-07-01"
title = "About me"
+++

I was born at an early age, then my two siblings were born, then I went to the University of New South Wales to get some tangible proof that I was an intelligent human being, then I got married and had six kids, while still clutching that belief that I was an intelligent human being.

Along the way I knitted things and made [a hymnbook](https://newbookoldhymns.brandt.id.au) and a course on [Singing the Little Office in Latin](https://littleoffice.brandt.id.au).

![Me in my yellow thurible hat](/meyellowhat.png)

